package com.isc.stuinfo.repository;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.isc.stuinfo.domain.Major;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.repository.domain.SortRequest.SortDirection;
import com.isc.stuinfo.repository.impl.AbstractCrudRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = "classpath:app-context.xml")
@ActiveProfiles("test")
public class AbstractCrudRepositoryTest {
	
	@Autowired 
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	// Open session for data interaction
	private Session openSession() {
		return sessionFactory.openSession();
	}
	
	@Before
	public void prepareData() {
		// Exec script - 'Cause @Sql doesn't work right
		SqlScriptExecUtil.execClassPathScript(this.dataSource, "mockup-data/Major.sql");
	}
	
	// Check if implementation of AbstractCrudRepository is injected or not
	@Test
	public void canInjectImplementation() {
		Assert.assertTrue("Autowire majorRepository successfully", majorRepository() != null);
	}
	
	// Get all data
	@Test
	@SuppressWarnings("unchecked")
	public void findAll() {
		// Get all data
		List<Major> testData = openSession().createCriteria(Major.class).list();
		List<Major> data = (List<Major>) majorRepository().findAll();

		// Let's do assertion :))
		Assert.assertTrue("Result is valid [" + data.size() + "]", testData.size() == data.size());
	}
	
	// Get data as pagination
	@Test
	@SuppressWarnings("unchecked")
	public void findAllAsPagination() {
		System.out.println("Find all as pagination");
		int pageNum = 0;
		int pageSize = 2;
		
		// Set page request
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		
		// Get data as pagination
		Page<Major> data = majorRepository().findAll(pageRequest);
		
		// Do some hibernate operation to check data
		Session session = this.openSession();
		
		// Data to check page request
		Long rowCount = (long)session.createCriteria(Major.class).setProjection(Projections.rowCount()).uniqueResult();
		Long totalPages = (long)Math.ceil(rowCount.doubleValue() / new Integer(pageSize).doubleValue());
		
		// Check content of page
		List<Major> testData = session.createCriteria(Major.class)
				.setFirstResult(pageNum)
				.setMaxResults(pageSize)
				.addOrder(Order.desc("id").ignoreCase())
				.list();
		
		// Let's do assertion
		// Assert page content
		List<Major> pageContent = (List<Major>)data.getContent();
		if(testData.size() == pageContent.size()) {
			Assert.assertTrue("Size of page content is invalid [" + testData.size() + "]", true);
			
			for(int i = 0; i < testData.size(); i++) {
				Assert.assertTrue("Major with id[" + i + "] is not equal to the other.", testData.get(i).equals(pageContent.get(i)));
			}
		}
		// Assert page metadata
		Assert.assertTrue("Page number is valid", data.getPageRequest().getPage() == pageNum);
		Assert.assertTrue("Page size is valid", data.getPageRequest().getSize() == pageSize);
		Assert.assertTrue("Sort request is valid", data.getSortRequest() == null);
		Assert.assertTrue("Total pages is valid", data.getTotalPages() == totalPages);
	}
	
	// Get data with sort
	@Test
	@SuppressWarnings("unchecked")
	public void findAllWithSort() {
		String sortProperty = "name";
		SortDirection sortDir = SortDirection.DESC;
		
		// Set sort request
		SortRequest sortRequest = new SortRequest(sortProperty, sortDir);
		
		// Get data
		List<Major> data = (List<Major>) majorRepository().findAll(sortRequest);
		
		// Use hibernate to build test data
		Session session = this.openSession();
		List<Major> testData = (List<Major>)session.createCriteria(Major.class)
				.addOrder(Order.desc(sortProperty).ignoreCase())	
				.addOrder(Order.desc("id").ignoreCase())			
				.list();
		
		// Do assertion
		if(data.size() == testData.size()) {
			Assert.assertTrue("Result size is valid", true);
			char prevChar = 1;
			
			for(int i = 0; i < data.size(); i++) {
				Major src = data.get(i);
				Major dest = testData.get(i);
				
				// Assert equal
				Assert.assertTrue("Major at index [" + i + "] is invalid", src.equals(dest));
				
				// Check order of sort property
				char currentChar = src.getName().toLowerCase().charAt(0);
				if(i >= 1) {
					Assert.assertTrue(currentChar <= prevChar);
					prevChar = currentChar;
				} else
					prevChar = currentChar;
			}
		}
	}	
	
	// Get data as pagination with sort
	@SuppressWarnings("unchecked")
	@Test
	public void findAllAsPaginationWithSort() {
		int pageNum = 0;
		int pageSize = 2;
		String sortProperty = "name";
		SortDirection sortDir = SortDirection.DESC;
		
		// Set PageRequest and SortRequest
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		SortRequest sortRequest = new SortRequest(sortProperty, sortDir); 	
		
		// Get data
		Page<Major> data = majorRepository().findAll(pageRequest, sortRequest);
		
		// Use hibernate to get test data
		Session session = this.openSession();
		List<Major> testData = session.createCriteria(Major.class)
				.addOrder(sortRequest.generateOrder().ignoreCase())
				.addOrder(Order.desc("id").ignoreCase())
				.setFirstResult(pageNum)
				.setMaxResults(pageSize)
				.list();
		
		Long rowCount = (long)session.createCriteria(Major.class).setProjection(Projections.rowCount()).uniqueResult();
		Long totalPages = (long)Math.ceil(rowCount.doubleValue() / new Integer(pageSize).doubleValue());
		
		// Assert page metadata
		Assert.assertTrue("Page number is valid", data.getPageRequest().getPage() == pageNum);
		Assert.assertTrue("Page size is valid", data.getPageRequest().getSize() == pageSize);
		Assert.assertTrue("Sort request is valid", data.getSortRequest() != null);
		Assert.assertTrue("Total pages is valid", data.getTotalPages() == totalPages);
		// Assert page content
		List<Major> temp = (List<Major>)data.getContent();
		if(temp.size() == testData.size()) {
			Assert.assertTrue("Result size is valid", true);
			char prevChar = 1;
			
			for(int i = 0; i < temp.size(); i++) {
				Major src = temp.get(i);
				Major dest = testData.get(i);
				
				// Assert equal
				Assert.assertTrue("Major at index [" + i + "] is valid", src.equals(dest));
				
				// Check order of sort property
				char firstChar = src.getName().toLowerCase().charAt(0);
				if(i >= 1)
					Assert.assertTrue(prevChar >= firstChar);
				else
					prevChar = firstChar;
			}
		}
	}

	// Get by id
	@Test
	public void findOne() {
		long id = 1;
		
		// Get ont
		Major major = majorRepository().findOne(id);
		
		// Use hibernate to check
		Session session = this.openSession();
		Major testMajor = (Major)session.createCriteria(Major.class).add(Restrictions.eq("id", id)).uniqueResult();
		
		Assert.assertTrue(testMajor.equals(major));
	}
	
	// Save 
	@Test
	public void save() {
		Major major = new Major();
		major.setName("abc");
		
		// Save
		majorRepository().save(major);
		
		// Use hibernate to check
		Session session = this.openSession();
		Object inst = session.createCriteria(Major.class).addOrder(Order.desc("id")).list().get(0);
		
		if(inst == null)
			Assert.assertTrue(false);
		
		Assert.assertTrue(major.equals((Major)inst));
	}
	
	// Update
	public void update() {
		long id = 1;
		Major major = majorRepository().findOne(id);
		
		// Update major
		major.setName("def");
		majorRepository().update(major);
		
		// Use hibernate to check
		Session session = this.openSession();
		Major updatedMajor = (Major)session.createQuery("select m from Major m where m.id = 1").uniqueResult();
		
		// Assertion
		Assert.assertTrue(updatedMajor.getName().equals("def"));
	}
	
	// Remove by id 
	public void removeById() {
		long id = 1;
		majorRepository().remove(id);
		
		// User hibernate to check
		Session session = this.openSession();
		Object inst = session.createQuery("select m from Major m where m.id = 1").uniqueResult();
		
		// Assertion
		Assert.assertTrue(inst == null);
	}
	
	// Remove by instance
	public void removeByInst() {
		Major major = new Major();
		major.setId(2);
		
		// Remove data
		majorRepository().remove(major);
		
		// Use hibernate to check
		Session session = this.openSession();
		Object inst = session.createQuery("select m from Major m where m.id = 2").uniqueResult();
		
		// Assertion
		Assert.assertTrue(inst == null);
	}
	
	/**
	 * 
	 * Config for testing
	 * 
	 */
	
	@Bean // Fake bean to test
	public MajorRepository majorRepository() {
		return new MajorRepositoryImpl(this.sessionFactory);
	}
	
	// Create implement of AbstractCrudRepository to do a test, just ignore it :))
	private interface MajorRepository extends CrudRepository<Major, Long> {}
	private class MajorRepositoryImpl extends AbstractCrudRepository<Major, Long> implements MajorRepository {
		public MajorRepositoryImpl(SessionFactory sessionFactory) {
			// TODO Auto-generated constructor stub
			super(sessionFactory);
		}
	}
}
