package com.isc.stuinfo.repository;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

public class SqlScriptExecUtil {

	// Execute SQL script exists on classpath
	public static void execClassPathScript(DataSource dataSource, String... paths) {
		List<ClassPathResource> scriptFiles = new ArrayList<>();
		for(String path : paths)
			scriptFiles.add(new ClassPathResource(path));
		exec(dataSource, scriptFiles.toArray(new ClassPathResource[scriptFiles.size()]));
	}
	
	// Execute script
	public static void exec(DataSource dataSource, Resource[] resources) {
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
	    populator.addScripts(resources);
	    populator.execute(dataSource);
	}
}
