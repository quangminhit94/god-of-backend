package com.isc.stuinfo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import com.isc.stuinfo.domain.Major;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.repository.domain.SortRequest.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(locations = {"classpath:web-context.xml", "classpath:app-context.xml"})
@WebAppConfiguration
public class MethodArgResolverTest {
	
	private MockMvc mockMvc;
	private MockMvcBuilders mockMvcBuilders;
	
	@Autowired
	private WebApplicationContext mockWebContext;
	
	@SuppressWarnings("static-access")
	@Before
	public void setup() {
		mockMvc = mockMvcBuilders.webAppContextSetup(mockWebContext).build();
	}
	
	/**
	 * 
	 * Positive test - no exceptions thrown
	 * 
	 */
	
	@Test
	public void findAllWitSort() throws Exception {
		mockMvc.perform(get("/test?sort=name,desc")).andReturn();
	}
	
	@Test
	public void findAllWithPaginationPageParamOnly() throws Exception {
		mockMvc.perform(get("/test?page=0")).andReturn();
	}
	
	@Test
	public void findAllWithPaginationFullParam() throws Exception {
		mockMvc.perform(get("/test?page=0&size=2")).andReturn();
	}
	
	@Test
	public void findAllWithPaginationPageParamOnlyAndSort() throws Exception {
		mockMvc.perform(get("/test?page=0&sort=name,desc")).andReturn();
	}
	
	@Test
	public void findWithPaginationFullParamAndSort() throws Exception {
		mockMvc.perform(get("/test?page=0&size=2&sort=name,desc")).andReturn();
	}
	
	/**
	 * Config controller for test
	 * @author tienp
	 *
	 */
	
	@Configuration
	@Profile("test")
	static class preTestConfig {
		// Major controller
		@Controller
		@RequestMapping("test")
		class MajorController implements QueryableController<Major, Long> {

			@Override
			public ResponseEntity<Iterable<Major>> findAll() {
				// TODO Auto-generated method stub
				// Do not need to be tested
				return null;
			}

			@Override
			public ResponseEntity<Iterable<Major>> findAll(SortRequest sortRequest) {
				// TODO Auto-generated method stub
				Assert.assertNotNull(sortRequest);
				sortRequestExpect(sortRequest, "name", SortDirection.DESC);
				return null;
			}

			@Override
			public ResponseEntity<Page<Major>> findAll(PageRequest pageRequest) {
				// TODO Auto-generated method stub
				Assert.assertNotNull(pageRequest);
				pageRequestExpect(pageRequest, 0, pageRequest.getSize());
				return null;
			}

			@Override
			public ResponseEntity<Page<Major>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
				// TODO Auto-generated method stub
				Assert.assertNotNull(pageRequest);
				Assert.assertNotNull(sortRequest);
				pageRequestExpect(pageRequest, 0, pageRequest.getSize());
				sortRequestExpect(sortRequest, "name", SortDirection.DESC);
				return null;
			}

			@Override
			public ResponseEntity<Major> findOne(Long id) {
				// TODO Auto-generated method stub
				// Don't need to be tested
				return null;
			}
			
			// PageRequest result - expectation
			private void pageRequestExpect(PageRequest pageRequest, int pageExpect, int sizeExpect) {
				Assert.assertEquals(pageExpect + sizeExpect, pageRequest.getPage() + pageRequest.getSize());
			}
			
			// SortRequest result - expectation
			private void sortRequestExpect(SortRequest sortRequest, String propExpect, SortDirection dirExpect) {
				Assert.assertEquals(propExpect, sortRequest.getProperty());
				Assert.assertTrue(sortRequest.getSortDirection() == dirExpect);
			}
		}
	}
	
}
