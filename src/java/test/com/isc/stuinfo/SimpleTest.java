package com.isc.stuinfo;

import static org.junit.Assert.assertTrue;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:app-context.xml")
@ActiveProfiles("test")
@Sql(scripts = {
	"classpath:mockup-data/Catalog.sql",
	"classpath:mockup-data/Major.sql",
	"classpath:mockup-data/Programe.sql",
	"classpath:mockup-data/Intake.sql",
	"classpath:mockup-data/RoomType.sql",
	"classpath:mockup-data/Room.sql",
	"classpath:mockup-data/CourseType.sql",
	"classpath:mockup-data/Course.sql",
	"classpath:mockup-data/Person.sql",
	"classpath:mockup-data/Instructor.sql",
	"classpath:mockup-data/Student.sql"
})
public class SimpleTest {

	@Autowired
	private SessionFactory sessionFactory;

	@Test
	public void runTest() {
		assertTrue(sessionFactory != null);
	}
}
