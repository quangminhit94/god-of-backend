insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ACTC', 2968, 'Basic and advanced Java programming', 1, '41.92', '1');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ACTB', 2271, 'Object oriented programming', 2, '67.70', '1');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ACTC', 2762, 'Database Design and Management', 3, '45.39', '1');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ACTD', 2091, 'Web Design', 4, '99.97', '1');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ACTE', 2445, 'Web Programming', 5, '62.86', '1');

insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ACTF', 2422, 'Mobile application programming', 6, '60.37', '1');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ECTA ', 2017, 'DESIGN CHIP AND SETTING YOUR SYSTEM', 15, '86.46', '2');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ECTB', 2041, 'DESIGN CHIP', 16, '85.17', '2');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('ECTC', 2545, 'PROGRAMMING THE SYSTEM', 17, '24.11', '2');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTA', 2992, 'Marketing Principles', 23, '48.65', '3');

insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTB', 2360, 'Market research', 25, '45.78', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTC', 2624, 'Planning Marketing Strategies', 26, '18.82', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTD', 2459, 'Harold', 27, '86.38', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTE', 2357, 'Sales Principles', 28, '57.06', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTF', 2912, 'Accounting principles', 29, '92.32', '3');

insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTG', 2410, 'Economic Law, Contract', 30, '53.72', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTH', 2951, 'Personal Finance', 31, '89.64', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTR', 2303, 'Behavior in the business', 32, '51.12', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTI', 2830, 'Small business administration', 33, '14.34', '3');
insert into Course (codeName, codeNumber, name, credits, cost, courseTypeId) values ('OCTK', 2498, 'English', 35, '55.60', '4');
