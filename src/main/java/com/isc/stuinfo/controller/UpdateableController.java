package com.isc.stuinfo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface UpdateableController<T, I> {
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public ResponseEntity<T> update(T entity, BindingResult bindingResult, I id);
}
