package com.isc.stuinfo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;

public interface QueryableController<T, I> {
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<T>> findAll();
	
	@RequestMapping(value = "", method = RequestMethod.GET, params = "sort")
	public ResponseEntity<Iterable<T>> findAll(SortRequest sortRequest);
	
	@RequestMapping(value = "", method = RequestMethod.GET, params = {"page"})
	public ResponseEntity<Page<T>> findAll(PageRequest pageRequest);
	
	@RequestMapping(value = "", method = RequestMethod.GET, params = {"page", "sort"})
	public ResponseEntity<Page<T>> findAll(PageRequest pageRequest, SortRequest sortRequest);
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<T> findOne(I id); 
}
