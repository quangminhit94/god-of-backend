package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Catalog;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.CatalogService;

@RestController
@RequestMapping("/catalogs")
public class CatalogController implements CrudController<Catalog, Long> {
	@Autowired
	private CatalogService catalogService;

	@Override
	public ResponseEntity<Iterable<Catalog>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Catalog>>(catalogService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Catalog>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Catalog>>(catalogService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Catalog>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Catalog>>(catalogService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Catalog>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Catalog>>(catalogService.findAll(pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Catalog> findOne(@PathVariable("id") Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Catalog>(catalogService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Catalog> save(@RequestBody @Valid Catalog entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		// Save entity
		catalogService.save(entity); 
		
		// Render uri for created resource
		String uri = uriBuilder.path("/catalogs/{id}")
				.buildAndExpand(entity.getId())
				.toString();
		
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		return new ResponseEntity<Catalog>(headers, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<Catalog> remove(@PathVariable Long id) {
		catalogService.remove(id);
		 return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Catalog> update(@RequestBody @Valid Catalog entity, BindingResult bindingResult,@PathVariable Long id) {
		// TODO Auto-generated method stub
		// Set id for entity
		entity.setId(id);
		
		// Update
		catalogService.update(entity);
		return new ResponseEntity<Catalog>(HttpStatus.OK);
	}
	
}
