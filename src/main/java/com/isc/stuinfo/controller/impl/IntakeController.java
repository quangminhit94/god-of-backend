package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Intake;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.IntakeService;

@Controller
@RequestMapping("intakes")
public class IntakeController implements CrudController<Intake, Long> {

	@Autowired
	private IntakeService intakeService;

	@Override
	public ResponseEntity<Iterable<Intake>> findAll() {
		return new ResponseEntity<>(intakeService.findAll(), HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<Iterable<Intake>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Page<Intake>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Page<Intake>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Intake> findOne(@PathVariable("id") Long id) {
		return new ResponseEntity<Intake>(intakeService.findOne(id),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Intake> save(@RequestBody @Valid Intake entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		intakeService.save(entity);

		// Render uri for created resource
		String uri = uriBuilder.path("/intakes/{id}").buildAndExpand(entity.getId()).toString();

		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		// Return
		return new ResponseEntity<Intake>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Intake> remove(@PathVariable("id") Long id) {
		intakeService.remove(id);
		 return new ResponseEntity<>(HttpStatus.OK);
	}
	@Override
	public ResponseEntity<Intake> update(@RequestBody @Valid Intake entity, BindingResult bindingResult, @PathVariable Long id) {
		entity.setId(id);
		intakeService.update(entity);
		// Return
		return new ResponseEntity<Intake>( HttpStatus.OK);
	}

}
