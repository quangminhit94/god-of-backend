package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Programe;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.ProgrameService;

@RestController
@RequestMapping(value = "/programes")
public class ProgrameController implements CrudController<Programe, Long> {
	
	@Autowired
	private ProgrameService programeService;

	@Override
	public ResponseEntity<Iterable<Programe>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Programe>>(
				programeService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Programe>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Programe>>(
				programeService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Programe>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Programe>>(
				programeService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Programe>> findAll(PageRequest pageRequest,
			SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Programe>>(programeService.findAll(
				pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Programe> findOne(@PathVariable("id") Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Programe>(programeService.findOne(id),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Programe> save(@RequestBody @Valid Programe entity, BindingResult bindingResult, 
			UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		programeService.save(entity);
		String uri = uriBuilder.path("/programes/{id}")
				.buildAndExpand(entity.getId()).toString();
		// Header
		HttpHeaders headers = new HttpHeaders();
		headers.add("location", uri);
		return new ResponseEntity<Programe>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Programe> remove(@PathVariable("id") Long id) {
		programeService.remove(id);
		return new ResponseEntity<Programe>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Programe> update(@RequestBody @Valid Programe entity, BindingResult bindingResult,
			@PathVariable Long id) {

		entity.setId(id);
		programeService.update(entity);
		return new ResponseEntity<Programe>(HttpStatus.OK);
	}
}
