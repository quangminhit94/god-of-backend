package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Student;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.StudentService;

@RestController
@RequestMapping("students")
public class StudentController implements CrudController<Student, Long>{
	
	@Autowired
	private StudentService studentService;
	
	@Override
	public ResponseEntity<Iterable<Student>> findAll() {
		return new ResponseEntity<Iterable<Student>>(studentService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Student>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Page<Student>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Page<Student>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Student> findOne(@PathVariable Long id) {
		return new ResponseEntity<Student>(studentService.findOne(id),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Student> save(@RequestBody @Valid Student entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		studentService.save(entity);
		String uri = uriBuilder.path("/students/{id}").buildAndExpand(entity.getId()).toString();
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		// Return
		return new ResponseEntity<Student>(headers, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<Student> remove(@PathVariable("id") Long id) {
		studentService.remove(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	@Override
	public ResponseEntity<Student> update(@RequestBody @Valid Student entity, BindingResult bindingResult, @PathVariable Long id) {
		entity.setId(id);
		studentService.update(entity);
		// Return
		return new ResponseEntity<Student>( HttpStatus.OK);
	}



}
