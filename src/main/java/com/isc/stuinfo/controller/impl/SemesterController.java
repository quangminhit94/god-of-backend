package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Semester;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.SemesterService;;

@RestController
@RequestMapping("semester")
public class SemesterController implements CrudController<Semester, Long> {

	@Autowired
	private SemesterService semesterService;

	@Override
	public ResponseEntity<Iterable<Semester>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Semester>>(semesterService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Semester>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Semester>>(semesterService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Semester>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Semester>>(semesterService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Semester>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Semester>>(semesterService.findAll(pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Semester> findOne(Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Semester>(semesterService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Semester> save(@RequestBody @Valid Semester entity, BindingResult bindingResult,
			UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		semesterService.save(entity);
		String uri = uriBuilder.path("/semester/{id}").buildAndExpand(entity.getId()).toString();
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		return new ResponseEntity<Semester>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Semester> remove(@PathVariable Long id) {
		// TODO Auto-generated method stub
		semesterService.remove(id);
		return new ResponseEntity<Semester>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Semester> update(@RequestBody @Valid Semester entity, BindingResult bindingResult, Long id) {
		// TODO Auto-generated method stub
		entity.setId(id);
		semesterService.update(entity);
		return new ResponseEntity<Semester>(HttpStatus.OK);
	}

}
