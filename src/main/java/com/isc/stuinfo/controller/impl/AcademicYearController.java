package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.AcademicYear;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.AcademicYearService;

@RestController
@RequestMapping(value = "/academic-years")
public class AcademicYearController implements CrudController<AcademicYear, Long> {
	@Autowired
	private AcademicYearService academicYearService;

	@Override
	public ResponseEntity<Iterable<AcademicYear>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<AcademicYear>>(
				academicYearService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<AcademicYear>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<AcademicYear>>(
				academicYearService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<AcademicYear>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<AcademicYear>>(
				academicYearService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<AcademicYear>> findAll(PageRequest pageRequest,
			SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<AcademicYear>>(academicYearService.findAll(
				pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<AcademicYear> findOne(@PathVariable("id") Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<AcademicYear>(academicYearService.findOne(id),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<AcademicYear> save(@RequestBody @Valid AcademicYear entity,
			BindingResult bindingResult,
			UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		academicYearService.save(entity);
		String uri = uriBuilder.path("/academic-years/{id}")
				.buildAndExpand(entity.getId()).toString();
		// Header
		HttpHeaders headers = new HttpHeaders();
		headers.add("location", uri);
		return new ResponseEntity<AcademicYear>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<AcademicYear> remove(@PathVariable("id") Long id) {
		academicYearService.remove(id);
		return new ResponseEntity<AcademicYear>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<AcademicYear> update(@RequestBody @Valid AcademicYear entity,
			BindingResult bindingResult,
			@PathVariable Long id) {

		entity.setId(id);
		academicYearService.update(entity);
		return new ResponseEntity<AcademicYear>(HttpStatus.OK);
	}
}
