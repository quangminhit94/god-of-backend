package com.isc.stuinfo.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.isc.stuinfo.domain.Student;
import com.isc.stuinfo.service.ImageService;
import com.isc.stuinfo.service.StudentService;

@RestController

public class UploadController {
	@Autowired
	private ImageService imageService;

	@Autowired
	private StudentService studentService;

	@RequestMapping(value = "students/{id}/avatar", method = RequestMethod.POST)
	public ResponseEntity<Object> uploadStudentAvatar(@RequestParam("file") MultipartFile image, @PathVariable long id) {

		Student student = studentService.findOne(id);
		if(student == null) { // If student with specific id not found
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			imageService.save(student, image);
			return new ResponseEntity<>(HttpStatus.OK);
		}

	}
	
}
