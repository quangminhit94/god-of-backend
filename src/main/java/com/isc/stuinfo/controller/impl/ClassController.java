package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Class;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.ClassService;

@RestController
@RequestMapping("/classes")
public class ClassController implements CrudController<Class, Long> {
	
	@Autowired
	private ClassService classService;

	@Override
	public ResponseEntity<Iterable<Class>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Class>>(classService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Class>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Class>>(classService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Class>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Class>>(classService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Class>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Class>>(classService.findAll(pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Class> findOne(Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Class>(classService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Class> save(@RequestBody @Valid Class entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		// Save entity
		classService.save(entity); 
		
		// Render uri for created resource
		String uri = uriBuilder.path("/classes/{id}")
				.buildAndExpand(entity.getId())
				.toString();
		
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		// Return
		return new ResponseEntity<Class>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Class> remove(Long id) {
		// TODO Auto-generated method stub
		// Remove
		classService.remove(id);
		return new ResponseEntity<Class>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Class> update(@RequestBody @Valid Class entity, BindingResult bindingResult, Long id) {
		// TODO Auto-generated method stub
		entity.setId(id);
		classService.update(entity);
		return new ResponseEntity<Class>(HttpStatus.OK);
	}

}
