package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Room;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.RoomService;

@RestController
@RequestMapping("/rooms")
public class RoomController implements CrudController<Room, Long> {
	
	@Autowired
	private RoomService roomService;
	

	@Override
	public ResponseEntity<Iterable<Room>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Room>>(roomService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Room>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<Room>>(roomService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Room>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Room>>(roomService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Room>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<Room>>(roomService.findAll(pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Room> findOne(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Room>(roomService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Room> save(@RequestBody @Valid Room entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		// Save entity
		roomService.save(entity); 
		
		// Render uri for created resource
		String uri = uriBuilder.path("/rooms/{id}")
				.buildAndExpand(entity.getId())
				.toString();
		
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		// Return
		return new ResponseEntity<Room>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Room> remove(@PathVariable Long id) {
		// TODO Auto-generated method stub
		// Remove
		roomService.remove(id);
		return new ResponseEntity<Room>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Room> update(@RequestBody @Valid Room entity, BindingResult bindingResult, @PathVariable Long id) {
		// TODO Auto-generated method stub
		entity.setId(id);
		roomService.update(entity);
		return new ResponseEntity<Room>(HttpStatus.OK);
	}

}
