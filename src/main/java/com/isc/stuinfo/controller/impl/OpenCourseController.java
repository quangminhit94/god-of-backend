package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.OpenCourse;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.OpenCourseService;

@RestController
@RequestMapping("open-courses")
public class OpenCourseController implements CrudController<OpenCourse, Long>{
	
	@Autowired
	private OpenCourseService OpenCourseService;
	
	@Override
	public ResponseEntity<Iterable<OpenCourse>> findAll() {
		return new ResponseEntity<Iterable<OpenCourse>>(OpenCourseService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<OpenCourse>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Page<OpenCourse>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Page<OpenCourse>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<OpenCourse> findOne(@PathVariable Long id) {
		return new ResponseEntity<OpenCourse>(OpenCourseService.findOne(id),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<OpenCourse> save(@RequestBody @Valid OpenCourse entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		OpenCourseService.save(entity);
		String uri = uriBuilder.path("/OpenCourses/{id}").buildAndExpand(entity.getId()).toString();
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		// Return
		return new ResponseEntity<OpenCourse>(headers, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<OpenCourse> remove(@PathVariable("id") Long id) {
		OpenCourseService.remove(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	@Override
	public ResponseEntity<OpenCourse> update(@RequestBody @Valid OpenCourse entity, BindingResult bindingResult, @PathVariable Long id) {
		entity.setId(id);
		OpenCourseService.update(entity);
		// Return
		return new ResponseEntity<OpenCourse>( HttpStatus.OK);
	}



}
