package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Major;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.MajorService;

@RestController
@RequestMapping(value="/majors")
public class MajorController implements CrudController<Major, Long> {
	@Autowired
	private MajorService majorService;
	@Override
	public ResponseEntity<Iterable<Major>> findAll() {
		return new ResponseEntity<Iterable<Major>>(majorService.findAll(),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<Major>> findAll(SortRequest sortRequest) {
		return new ResponseEntity<Iterable<Major>>(majorService.findAll(sortRequest),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Major>> findAll(PageRequest pageRequest) {
		return new ResponseEntity<Page<Major>>(majorService.findAll(pageRequest),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Major>> findAll(PageRequest pageRequest,
			SortRequest sortRequest) {
		return new ResponseEntity<Page<Major>>(majorService.findAll(pageRequest, sortRequest),HttpStatus.OK);
		}

	@Override
	public ResponseEntity<Major> findOne(@PathVariable("id") Long id) {
		return new ResponseEntity<Major>(majorService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Major> save(@RequestBody @Valid Major entity, BindingResult bindingResult, 
			UriComponentsBuilder uriBuilder) {
		// Save entity
		majorService.save(entity); 
				
		// Render uri for created resource
		String uri = uriBuilder.path("/majors/{id}")
				.buildAndExpand(entity.getId())
				.toString();
		
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		return new ResponseEntity<Major>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Major> remove(@PathVariable("id") Long id) {
		// TODO Auto-generated method stub
		majorService.remove(id);
		return new ResponseEntity<Major>(HttpStatus.OK);
	}

	public ResponseEntity<Major> update(@RequestBody @Valid Major entity, BindingResult bindingResult, @PathVariable Long id) {
		// Set id for entity
		entity.setId(id);
		
		// Update
		majorService.update(entity);
		return new ResponseEntity<Major>(HttpStatus.OK);
	}

}
