package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.Course;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.CourseService;

@Controller
@RequestMapping("courses")
public class CourseController implements CrudController<Course, Long> {

	@Autowired
	private CourseService courseService;
	@Override
	public ResponseEntity<Iterable<Course>> findAll() {
		return new ResponseEntity<Iterable<Course>>(courseService.findAll(), HttpStatus.OK);

	}

	@Override
	public ResponseEntity<Iterable<Course>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub

		return new ResponseEntity<Iterable<Course>>(courseService.findAll(sortRequest),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Course>> findAll(PageRequest pageRequest) {


		return new ResponseEntity<Page<Course>>(courseService.findAll(pageRequest),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<Course>> findAll(PageRequest pageRequest, SortRequest sortRequest) {


		return new ResponseEntity<Page<Course>>(courseService.findAll(pageRequest, sortRequest),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Course> findOne(@PathVariable Long id) {
		return new ResponseEntity<Course>(courseService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Course> save(@RequestBody @Valid Course entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		courseService.save(entity);
		String uri=uriBuilder.path("/course-types/{id}")
				.buildAndExpand(entity.getId()).toString();
		//Header
		HttpHeaders headers = new HttpHeaders();
		headers.add("location",	uri);
		return new ResponseEntity<Course>(headers,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Course> remove(@PathVariable Long id) {
			courseService.remove(id);
		 return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Course> update(@RequestBody @Valid Course entity, BindingResult bindingResult, @PathVariable Long id) {
		entity.setId(id);
		courseService.update(entity);
		return new ResponseEntity<Course>(HttpStatus.OK);
	}

}
