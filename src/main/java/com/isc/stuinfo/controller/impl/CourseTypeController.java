package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.CourseType;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.CourseTypeService;

@RestController
@RequestMapping(value = "/course-types")
public class CourseTypeController implements CrudController<CourseType, Long> {
	@Autowired
	private CourseTypeService coursetypeservice;

	@Override
	public ResponseEntity<Iterable<CourseType>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<CourseType>>(coursetypeservice.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<CourseType>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<CourseType>>(coursetypeservice.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<CourseType>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<CourseType>>(coursetypeservice.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<CourseType>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<CourseType>>(coursetypeservice.findAll(pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CourseType> findOne(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<CourseType>(coursetypeservice.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CourseType> save(@RequestBody @Valid CourseType entity, BindingResult bindingResult,
			UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		coursetypeservice.save(entity);
		String uri = uriBuilder.path("/course-types/{id}").buildAndExpand(entity.getId()).toString();
		// Header
		HttpHeaders headers = new HttpHeaders();
		headers.add("location", uri);
		return new ResponseEntity<CourseType>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CourseType> remove(@PathVariable Long id) {
		// TODO Auto-generated method stub
		coursetypeservice.remove(id);
		return new ResponseEntity<CourseType>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CourseType> update(@RequestBody @Valid CourseType entity, BindingResult bindingResult,
			@PathVariable Long id) {
		// TODO Auto-generated method stub
		entity.setId(id);
		coursetypeservice.update(entity);
		return new ResponseEntity<CourseType>(HttpStatus.OK);
	}
}
