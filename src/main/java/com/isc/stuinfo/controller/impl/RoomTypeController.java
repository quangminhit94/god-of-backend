package com.isc.stuinfo.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.isc.stuinfo.controller.CrudController;
import com.isc.stuinfo.domain.RoomType;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.RoomTypeService;

@RestController
@RequestMapping("/room-types")
public class RoomTypeController implements CrudController<RoomType, Long> {
	
	@Autowired
	private RoomTypeService roomService;

	@Override
	public ResponseEntity<Iterable<RoomType>> findAll() {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<RoomType>>(roomService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Iterable<RoomType>> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Iterable<RoomType>>(roomService.findAll(sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<RoomType>> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<RoomType>>(roomService.findAll(pageRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Page<RoomType>> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Page<RoomType>>(roomService.findAll(pageRequest, sortRequest), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<RoomType> findOne(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return new ResponseEntity<RoomType>(roomService.findOne(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<RoomType> save(@RequestBody @Valid RoomType entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder) {
		// TODO Auto-generated method stub
		// Save entity
		roomService.save(entity); 
		
		// Render uri for created resource
		String uri = uriBuilder.path("/room-types/{id}")
				.buildAndExpand(entity.getId())
				.toString();
		
		// Headers
		HttpHeaders headers = new HttpHeaders(); // New headers
		headers.add("location", uri);
		// Return
		return new ResponseEntity<RoomType>(headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<RoomType> remove(@PathVariable Long id) {
		// TODO Auto-generated method stub
		// Remove
		roomService.remove(id);
		return new ResponseEntity<RoomType>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<RoomType> update(@RequestBody @Valid RoomType entity, BindingResult bindingResult, @PathVariable Long id) {
		// TODO Auto-generated method stub
		entity.setId(id);
		roomService.update(entity);
		return new ResponseEntity<RoomType>(HttpStatus.OK);
	}

}
