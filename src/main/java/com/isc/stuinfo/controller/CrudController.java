package com.isc.stuinfo.controller;

public interface CrudController<T, I> extends QueryableController<T, I>, InsertableController<T>,
		RemoveableController<T, I>, UpdateableController<T, I> {

}
