package com.isc.stuinfo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

public interface InsertableController<T> {
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<T> save(T entity, BindingResult bindingResult, UriComponentsBuilder uriBuilder);
}
