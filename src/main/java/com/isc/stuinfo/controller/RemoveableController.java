package com.isc.stuinfo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface RemoveableController<T, I> {
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<T> remove(I id);
}
