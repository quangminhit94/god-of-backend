package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.CourseType;

public interface CourseTypeRepository extends
		CrudRepository<CourseType, Long> {

}
