package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Intake;

public interface IntakeRepository extends CrudRepository<Intake, Long> {

}
