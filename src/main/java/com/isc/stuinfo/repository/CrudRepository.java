package com.isc.stuinfo.repository;

import java.io.Serializable;

import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;

/**
 * Implement this interface for doing Crud operations
 * @author tienp
 *
 * @param <T> entity type 
 * @param <I> id type of entity (must extends from java.io.Serializable)
 */
public interface CrudRepository<T, I extends Serializable> {
	
	/**
	 * Find all
	 * @return data as list
	 */
	public Iterable<T> findAll();
	
	/**
	 * Find all with sort property
	 * @param sort meta data for sorting
	 * @return data as list
	 */
	public Iterable<T> findAll(SortRequest sort);
	
	/**
	 * Find all with pagination
	 * @param page metadata for pagination
	 * @return data as list
	 */
	public Page<T> findAll(PageRequest page);
	
	/**
	 * Find all with pagination along with sort
	 * @param page metadata for pagination such as page number, size
	 * @param sort metadata for sort
	 * @return data as list
	 */
	public Page<T> findAll(PageRequest page, SortRequest sort);
	
	/**
	 * Find data with specific id
	 * @param id id
	 * @return data as list
	 */
	public T findOne(I id);
	
	/**
	 * Save entity
	 * @param entity entity that need to be saved
	 */
	public void save(T entity);
	
	/**
	 * Update entity
	 * @param entity entity that need to be updated
	 */
	public void update(T entity);
	
	/**
	 * Remove by specific id
	 * @param id id
	 */
	public void remove(I id);
	
	/**
	 * Remove by instance
	 * @param entity entity
	 */
	public void remove(T entity);
}
