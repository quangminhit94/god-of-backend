package com.isc.stuinfo.repository.domain;

import org.hibernate.criterion.Order;

/**
 * Sort metadata provided by client
 * @author tienp
 *
 */
public class SortRequest {

	public enum SortDirection {
		ASC, DESC;
	}

	private String property;
	private SortDirection sortDirection;

	public SortRequest(String property, SortDirection direction) {
		this.property = property;
		this.sortDirection = direction;
	}

	public String getProperty() {
		return property;
	}

	public SortDirection getSortDirection() {
		return sortDirection;
	}

	public Order generateOrder() {
		if(sortDirection.equals(SortDirection.ASC))
			return Order.asc(property);
		return Order.desc(property);
	}
}