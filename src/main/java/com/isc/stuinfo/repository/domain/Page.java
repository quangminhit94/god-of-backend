package com.isc.stuinfo.repository.domain;

/**
 * Store data content along with metadata
 * @author tienp
 */
public class Page<T> {

	private PageRequest pageRequest;
	private SortRequest sortRequest;
	private long totalPages;
	private boolean lastPage;
	private Iterable<T> content;

	public Page(PageRequest pageRequest, SortRequest sortRequest, boolean lastPage, long totalPages, Iterable<T> content) {
		this.pageRequest = pageRequest;
		this.sortRequest = sortRequest;
		this.lastPage = lastPage;
		this.totalPages = totalPages;
		this.content = content;
	}

	public PageRequest getPageRequest() {
		return pageRequest;
	}

	public SortRequest getSortRequest() {
		return sortRequest;
	}

	public boolean isLastPage() {
		return lastPage;
	}

	public Iterable<T> getContent() {
		return content;
	}
	
	public long getTotalPages() {
		return totalPages;
	}
	
	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}
}
