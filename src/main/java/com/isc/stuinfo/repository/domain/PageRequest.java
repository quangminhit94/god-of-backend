package com.isc.stuinfo.repository.domain;

/**
 * Metadata for pagination provided by client
 * @author tienp
 *
 */
public class PageRequest {
	private final int DEFAULT_SIZE = 20;

	private int page = 0;
	private int size = DEFAULT_SIZE;

	public PageRequest(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public PageRequest(int page) {
		this.page = page;
	}

	public int getPage() {
		return page;
	}

	public int getSize() {
		return size;
	}
}
