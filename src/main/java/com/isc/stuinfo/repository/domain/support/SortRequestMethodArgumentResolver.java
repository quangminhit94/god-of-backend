package com.isc.stuinfo.repository.domain.support;

import java.io.IOException;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.repository.domain.SortRequest.SortDirection;

public class SortRequestMethodArgumentResolver implements HandlerMethodArgumentResolver {

	private final String SORT_PARAM_NAME = "sort"; // Default sort paramater name
	private final String DELIMITER = ","; // Delimiter of sort value
	 
	private final int SORT_PROP_INX = 0; // Default position of sort property
	private final int SORT_DIR_INX = 1; // Default position of sort direction
	
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		// TODO Auto-generated method stub
		return parameter.getParameterType().equals(SortRequest.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		// TODO Auto-generated method stub
		String paramVal = webRequest.getParameter(SORT_PARAM_NAME);
		if(paramVal == null)
			throw new IOException("Sort parameter not found.");
		
		return parse(paramVal);
	}
	
	private SortRequest parse(String paramVal) throws IOException {
		String sortProp = null;
		SortDirection sortDir = null;
		
		// Split param value to get sort property and sort direction
		String[] splitedStr = paramVal.split(DELIMITER);
		
		// Validate
		if(splitedStr.length != 2)
			throw new IOException("Sort param structure is invalid.");
		
		// Init value
		try {
			sortProp = splitedStr[SORT_PROP_INX];
			sortDir = SortDirection.valueOf(splitedStr[SORT_DIR_INX].toUpperCase());
			return new SortRequest(sortProp, sortDir);
		} catch(Exception ex) {
			throw new IOException("Sort param value is invalid.");
		}
	}

}
