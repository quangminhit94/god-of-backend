package com.isc.stuinfo.repository.domain.support;

import java.io.IOException;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.isc.stuinfo.repository.domain.PageRequest;

public class PageRequestMethodArgumentResolver implements HandlerMethodArgumentResolver {
	
	private final String PAGE_PARAM_NAME = "page";
	private final String SIZE_PARAM_NAME = "size";

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		// TODO Auto-generated method stub
		return parameter.getParameterType().equals(PageRequest.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		// TODO Auto-generated method stub
		String pageStr = webRequest.getParameter(PAGE_PARAM_NAME);
		String sizeStr =webRequest.getParameter(SIZE_PARAM_NAME);
		
		if(pageStr == null) // Prevent from null value from page param
			throw new IOException("Page paramter not found.");
		return parse(pageStr, sizeStr);
	}

	private PageRequest parse(String pageStr, String sizeStr) throws IOException {
		try {
			int page = Integer.valueOf(pageStr); // Parse value
			
			if(page < 0) // Validate page value
				throw new IOException("Page param value is invalid");
			
			if(sizeStr != null) {
				int size = Integer.valueOf(sizeStr); // Parse value
				if(size < 1) // Validate size value
					throw new IOException("Size param value is invalid.");
				
				return new PageRequest(page, size);
			} else 
				return new PageRequest(page);
			
		} catch(Exception ex) {
			throw new IOException("Page and Size params are invalid.");
		}
	}
}
