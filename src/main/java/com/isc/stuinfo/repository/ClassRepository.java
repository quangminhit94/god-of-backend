package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Class;

public interface ClassRepository extends CrudRepository<Class, Long> {

}
