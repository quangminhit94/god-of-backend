package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Instructor;

public interface InstructorRepository extends CrudRepository<Instructor, Long> {

}
