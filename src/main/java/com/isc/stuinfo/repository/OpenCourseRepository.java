package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.OpenCourse;

public interface OpenCourseRepository extends CrudRepository<OpenCourse, Long> {

}
