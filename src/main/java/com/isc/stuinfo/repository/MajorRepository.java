package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Major;

public interface MajorRepository extends CrudRepository<Major, Long> {

}
