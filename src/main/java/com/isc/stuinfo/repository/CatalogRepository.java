package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Catalog;

public interface CatalogRepository extends CrudRepository<Catalog, Long> {

}
