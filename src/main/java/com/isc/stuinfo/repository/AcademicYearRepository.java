package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.AcademicYear;

public interface AcademicYearRepository extends CrudRepository<AcademicYear, Long> {

}
