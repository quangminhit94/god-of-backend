package com.isc.stuinfo.repository;

import org.springframework.stereotype.Repository;

import com.isc.stuinfo.domain.Student;
import com.isc.stuinfo.service.CrudService;

@Repository
public interface StudentReponsitory  extends CrudService<Student, Long>{

}
