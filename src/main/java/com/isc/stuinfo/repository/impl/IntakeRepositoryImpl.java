package com.isc.stuinfo.repository.impl;

import com.isc.stuinfo.domain.Intake;
import com.isc.stuinfo.repository.IntakeRepository;

import org.springframework.stereotype.Repository;

@Repository
public class IntakeRepositoryImpl extends AbstractCrudRepository<Intake, Long> implements IntakeRepository {

}
