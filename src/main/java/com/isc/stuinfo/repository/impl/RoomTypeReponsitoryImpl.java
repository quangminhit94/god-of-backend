package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;
import com.isc.stuinfo.domain.RoomType;
import com.isc.stuinfo.repository.RoomTypeReponsitory;

@Repository
public class RoomTypeReponsitoryImpl extends AbstractCrudRepository<RoomType, Long> implements RoomTypeReponsitory {

}
