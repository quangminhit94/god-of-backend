package com.isc.stuinfo.repository.impl;

import com.isc.stuinfo.domain.Major;
import com.isc.stuinfo.repository.MajorRepository;

import org.springframework.stereotype.Repository;

@Repository
public class MajorRepositoryImpl extends AbstractCrudRepository<Major, Long> implements MajorRepository {

}
