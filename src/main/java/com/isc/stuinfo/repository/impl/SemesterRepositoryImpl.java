package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;

import com.isc.stuinfo.domain.Semester;
import com.isc.stuinfo.repository.SemesterRepository;


@Repository
public class SemesterRepositoryImpl extends AbstractCrudRepository<Semester, Long> implements SemesterRepository {

}
