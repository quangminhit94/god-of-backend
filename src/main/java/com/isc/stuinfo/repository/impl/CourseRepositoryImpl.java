package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;
import com.isc.stuinfo.domain.Course;
import com.isc.stuinfo.repository.CourseRepository;

@Repository
public class CourseRepositoryImpl extends AbstractCrudRepository<Course, Long>
		implements CourseRepository {

}
