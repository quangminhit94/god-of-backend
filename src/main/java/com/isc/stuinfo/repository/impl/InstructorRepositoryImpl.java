package com.isc.stuinfo.repository.impl;

import com.isc.stuinfo.domain.Instructor;
import com.isc.stuinfo.repository.InstructorRepository;

import org.springframework.stereotype.Repository;

@Repository
public class InstructorRepositoryImpl extends AbstractCrudRepository<Instructor, Long> implements InstructorRepository {

}
