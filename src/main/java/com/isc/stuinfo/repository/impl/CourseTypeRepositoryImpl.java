package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;
import com.isc.stuinfo.domain.CourseType;
import com.isc.stuinfo.repository.CourseTypeRepository;

@Repository
public class CourseTypeRepositoryImpl extends
		AbstractCrudRepository<CourseType, Long> implements
		CourseTypeRepository {

}
