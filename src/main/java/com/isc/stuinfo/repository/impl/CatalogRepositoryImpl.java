package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;

import com.isc.stuinfo.domain.Catalog;
import com.isc.stuinfo.repository.CatalogRepository;

@Repository
public class CatalogRepositoryImpl extends
		AbstractCrudRepository<Catalog, Long> implements CatalogRepository {
}
