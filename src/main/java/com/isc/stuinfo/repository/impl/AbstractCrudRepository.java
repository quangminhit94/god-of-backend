package com.isc.stuinfo.repository.impl;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.transaction.annotation.Transactional;

import com.isc.stuinfo.repository.CrudRepository;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;

/**
 * If you want basic Crud available in your repo, please extends this class (@Id or @EmbeddedId must be placed on field level)
 * @author tienp
 *
 * @param <T>
 * @param <I>
 */
@Transactional
public abstract class AbstractCrudRepository<T, I extends Serializable> implements CrudRepository<T, I> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Class<T> domainType;
    private Field idField;

    public AbstractCrudRepository() {
		// TODO Auto-generated constructor stub
    	init();
	}
    
    public AbstractCrudRepository(SessionFactory sessionFactory) {
    	init();
    	this.sessionFactory = sessionFactory;
    }
    
    // Access generic params of implementation
    public void init() {
    	domainType = getDomainType();
        
    	AnnotatedElement annoEle = findIdElement();
        if(annoEle instanceof Field)
        	idField = (Field)annoEle;
        else throw new RuntimeException("idField property must be a field!");
    }

    /**
     * Get annotated element (field or method) - Getter / Setter is not supported
     * @return
     */
    private AnnotatedElement findIdElement() {
        // Get domain type
        Class<T> type = getDomainType();

        // Find element
        List<Class<?>> classes = getSuperClasses(type);

        // Found element annotated with javax.persistence.Id.class or javax.persistence.EmbeddedId.class
        AnnotatedElement element = null;
        for(Class<?> currentClass : classes)
             element = findIdElementInFields(currentClass);

        if(element == null)
            throw new RuntimeException("No fields are annotated with javax.persistence.Id.class or javax.persistence.EmbeddedId.class");
        return element;
    }

    /**
     * Get list of super classes of current domain type
     * @param type
     * @return
     */
    private List<Class<?>> getSuperClasses(Class<T> type) {
        // Init super class storage
        List<Class<?>> superClasses = new ArrayList<Class<?>>();

        // Get super class recursively
        Class<?> currentClass = type;
        while(!currentClass.equals(Object.class)) {
            superClasses.add(currentClass);
            currentClass = currentClass.getSuperclass();
        }

        return superClasses;
    }

    /**
     * Find field annotated with javax.persistence.Id or javax.persistence.EmbeddedId
     * @param type
     * @return
     */
    private AnnotatedElement findIdElementInFields(Class<?> type) {
        Field fields[] = type.getDeclaredFields();
        for(Field field : fields)
            if(isHasIdAnnotation(field.getAnnotations()))
                return field;
        return null;
    }

    /**
     * Get domain type
     */
    private Class<T> getDomainType() {
        return getType(0);
    }

    /**
     * Get type from generic class parameters
     * @param index index of generic parameter
     * @param <E> T as Domain Type or ID as Id Type
     * @return
     */
    @SuppressWarnings("unchecked")
	private <E> Class<E> getType(int index) {
        Class<?> types[] = GenericTypeResolver.resolveTypeArguments(this.getClass(), AbstractCrudRepository.class);
        return (Class<E>) types[index];
    }

    /**
     * Check javax.persistence.Id or javax.persistence.EmbeddedId available in the list of annotations
     * @param annotations Field's annotations
     * @return
     */
    private boolean isHasIdAnnotation(Annotation annotations[]) {
        for(Annotation annotation : annotations) {
            Class<? extends Annotation> type = annotation.annotationType();
            if(type.equals(Id.class) || type.equals(EmbeddedId.class))
                return true;
        }
        return false;
    }
	
    // If no session is available, new session will be created
	protected Session getSession() {
		try {
			return sessionFactory.getCurrentSession();
		} catch(Exception ex) {
			return sessionFactory.openSession();
		}
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Iterable<T> findAll() {
		// TODO Auto-generated method stub
		return addSort(this.getSession().createCriteria(domainType), null).list();
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Iterable<T> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return addSort(this.getSession().createCriteria(domainType), sortRequest).list();
	}

	@Transactional(readOnly = true)
	public Page<T> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return findAll(pageRequest, null);
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Page<T> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		// Generate criteria
		Session session = this.getSession();
		Criteria criteria = session.createCriteria(domainType);
		
		criteria = addPagination(criteria, pageRequest);
		criteria = addSort(criteria, sortRequest);
		
		// Count all row of table
		Long rowCount = (long) this.getSession().createCriteria(domainType)
				.setProjection(Projections.rowCount())
				.uniqueResult();
		
		// Check if lastPage is, and calculate total page
		Long currentPage = (long) (pageRequest.getPage() + 1);
		Long size = (long) (pageRequest.getSize());
		Long totalPages = (long) Math.ceil(rowCount.doubleValue() / size.doubleValue());
		
		// Return result
		return new Page<T>(pageRequest, 
				sortRequest, 
				currentPage == totalPages, 
				totalPages,
				criteria.list());
	}
	
	// Add pagination into criteria
	private Criteria addPagination(Criteria criteria, PageRequest pageRequest) {
		if(pageRequest == null)
			return criteria;
		return criteria.setFirstResult(pageRequest.getPage())
				.setMaxResults(pageRequest.getSize());
	}
	
	// Add sort/order into criteria - add order by id as default order - ignoreCase by default
	private Criteria addSort(Criteria criteria, SortRequest sortRequest) {
		Order idOrder = Order.desc(idField.getName()).ignoreCase();
		if(sortRequest == null)
			return criteria.addOrder(idOrder);
		return criteria.addOrder(Order.desc(sortRequest.getProperty()).ignoreCase())
				.addOrder(idOrder);
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public T findOne(I id) {
		// TODO Auto-generated method stub
		return (T)getSession().createCriteria(domainType)
				.add(Restrictions.eq(idField.getName(), id))
				.uniqueResult();
	}

	public void save(T entity) {
		// TODO Auto-generated method stub
		getSession().save(entity);
	}

	public void update(T entity) {
		// TODO Auto-generated method stub
		getSession().update(entity);
	}

	@Override
	public void remove(I id) {
		// TODO Auto-generated method stub
		T instance = findOne(id);
		getSession().delete(instance);
	}

	@Override
	public void remove(T entity) {
		// TODO Auto-generated method stub
		getSession().delete(entity);
	}
}
