package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;
import com.isc.stuinfo.domain.Class;
import com.isc.stuinfo.repository.ClassRepository;

@Repository
public class ClassRepositoryImpl extends AbstractCrudRepository<Class, Long>
		implements ClassRepository {

}
