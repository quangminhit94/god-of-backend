package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;

import com.isc.stuinfo.domain.Programe;
import com.isc.stuinfo.repository.ProgrameRepository;

@Repository
public class ProgrameRepositoryImpl extends AbstractCrudRepository<Programe, Long> implements ProgrameRepository {

}
