package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;

import com.isc.stuinfo.domain.OpenCourse;
import com.isc.stuinfo.repository.OpenCourseRepository;

@Repository
public class OpenCourseRepositoryImpl extends AbstractCrudRepository<OpenCourse, Long> implements OpenCourseRepository {

}
