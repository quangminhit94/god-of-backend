package com.isc.stuinfo.repository.impl;

import org.springframework.stereotype.Repository;

import com.isc.stuinfo.domain.AcademicYear;
import com.isc.stuinfo.repository.AcademicYearRepository;

@Repository
public class AcademicYearRepositoryImpl extends
		AbstractCrudRepository<AcademicYear, Long> implements AcademicYearRepository {
}
