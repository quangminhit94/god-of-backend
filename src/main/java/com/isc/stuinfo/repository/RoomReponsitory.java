package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Room;
import com.isc.stuinfo.service.CrudService;

public interface RoomReponsitory extends CrudService<Room, Long> {

}
