package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Semester;
import com.isc.stuinfo.service.CrudService;

public interface SemesterRepository extends CrudService<Semester, Long>{

}
