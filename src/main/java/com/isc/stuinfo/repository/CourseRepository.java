package com.isc.stuinfo.repository;

import com.isc.stuinfo.domain.Course;

public interface CourseRepository extends CrudRepository<Course, Long> {

}
