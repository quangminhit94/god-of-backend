package com.isc.stuinfo.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonCreator;

@Entity
@Table(
	name = "openCourse",
	uniqueConstraints = @UniqueConstraint(columnNames = {"semesterId", "academicYearId"})
)
public class OpenCourse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JoinColumn(name = "semesterId")
	private Semester semester;

	@ManyToOne
	@JoinColumn(name = "academicYearId")
	private AcademicYear academicYear;

	@ManyToMany
	@JoinTable(
		name = "OpenCourseCourse", 
		joinColumns = @JoinColumn(name = "openCourseId"), 
		inverseJoinColumns = @JoinColumn(name = "courseId"),
		uniqueConstraints = @UniqueConstraint(columnNames = {"openCourseId", "courseId"})
	)
	private Set<Course> courses = new HashSet<>();

	public OpenCourse() {

	}

	@SuppressWarnings("unchecked")
	@JsonCreator
	public OpenCourse(Map<String, Object> props) {
		Semester semester = new Semester();
		semester.setId((int) props.get("semester"));
		this.semester = semester;

		AcademicYear academicYear = new AcademicYear();
		academicYear.setId((int) props.get("academicYear"));
		this.academicYear = academicYear;

		List<Integer> ids = (List<Integer>) props.get("courses");
		ids.forEach((id) -> {
			Course course = new Course();
			course.setId(id);
			this.courses.add(course);
		});
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourse(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OpenCourse other = (OpenCourse) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
