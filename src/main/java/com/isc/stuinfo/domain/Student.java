package com.isc.stuinfo.domain;


import java.text.ParseException;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:33:35 PM
 */
@Entity
@Table(name = "student")
public class Student extends Person {
	@Column(name = "hccsPid")
	@NotNull
	@NotEmpty
	private String hccsPid;
	
	@Column(name = "hccsEmpid")
	@NotNull
	@NotEmpty
	private String hccsEmpid;
	
	@ManyToOne
	@JoinColumn(name = "catalogId")
	@NotNull
	private Catalog catalog;
	
	@Column(name = "homeCountry")
	@NotNull
	@NotEmpty
	private String homeCountry;
	
	@Column(name = "placeOfBirth")
	@NotNull
	@NotEmpty
	private String placeOfBirth;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "intakeId")
	@NotNull
	private Intake intake;
	
	@ManyToOne
	@JoinColumn(name = "programeId")
	@NotNull
	private Programe programe;
	
	@Column(name = "contactPerson")
	@NotNull
	@NotEmpty
	private String contactPerson;
	
	@Column(name = "personalId")
	@NotNull
	@NotEmpty
	private String personalId;
	
	@Column(name = "hobby")
	@NotNull
	@NotEmpty
	private String hobby;

	@JsonCreator
	public Student(Map<String, Object> props) throws ParseException {
		super(props);
		this.hccsPid = (String)props.get("hccsPid");
		this.hccsEmpid = (String)props.get("hccsEmpid");
		this.homeCountry = (String)props.get("homeCountry");
		this.placeOfBirth = (String)props.get("placeOfBirth");
		this.contactPerson = (String)props.get("contactPerson");
		this.personalId = (String)props.get("personalId");
		this.hobby = (String)props.get("hobby");
		
		Intake intake = new Intake();
		intake.setId((int)props.get("intake"));
		this.intake = intake;
		
		Catalog catalog = new Catalog();
		catalog.setId((int)props.get("catalog"));
		this.catalog = catalog;
		
		Programe programe = new Programe();
		programe.setId((int)props.get("programe"));
		this.programe = programe;
	}
	
	public Student() {

	}

	public String getHccsPid() {
		return hccsPid;
	}

	public void setHccsPid(String hccsPid) {
		this.hccsPid = hccsPid;
	}

	public String getHccsEmpid() {
		return hccsEmpid;
	}

	public void setHccsEmpid(String hccsEmpid) {
		this.hccsEmpid = hccsEmpid;
	}

	public Catalog getCatalog() {
		return catalog;
	}

	public void setCatalog(Catalog catalog) {
		this.catalog = catalog;
	}

	public String getHomeCountry() {
		return homeCountry;
	}

	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public Intake getIntake() {
		return intake;
	}

	public void setIntake(Intake intake) {
		this.intake = intake;
	}

	public Programe getPrograme() {
		return programe;
	}

	public void setProgram(Programe programe) {
		this.programe = programe;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public void setPrograme(Programe programe) {
		this.programe = programe;
	}
}