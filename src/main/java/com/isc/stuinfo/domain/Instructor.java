package com.isc.stuinfo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:38:03 PM
 */
@Entity
@Table(name = "instructor")
public class Instructor extends Person {

	@Column(name = "barcode")
	@NotNull
	@NotEmpty
	private String barcode;
	
	@ManyToOne
	@JoinColumn(name = "majorId")
	@NotNull
	private Major major;
	
	@Column(name = "highestDegree")
	@NotNull
	@NotEmpty
	private String highestDegree;
	
	@Column(name = "foreigner")
	private boolean foreigner;
	
	@Column(name = "active")
	private boolean active;

	public Instructor() {

	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public String getHighestDegree() {
		return highestDegree;
	}

	public void setHighestDegree(String highestDegree) {
		this.highestDegree = highestDegree;
	}

	public boolean isForeigner() {
		return foreigner;
	}

	public void setForeigner(boolean foreigner) {
		this.foreigner = foreigner;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}