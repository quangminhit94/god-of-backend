package com.isc.stuinfo.domain.converter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by tienp on 3/25/2017.
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {
        return attribute == null? null: Timestamp.valueOf(attribute);
    }

    public LocalDateTime convertToEntityAttribute(Timestamp dbData) {
        return dbData == null? null: dbData.toLocalDateTime();
    }

}
