package com.isc.stuinfo.domain.converter;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by tienp on 3/25/2017.
 */
@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

    public Date convertToDatabaseColumn(LocalDate attribute) {
        return attribute == null? null: Date.valueOf(attribute);
    }

    public LocalDate convertToEntityAttribute(Date dbData) {
        return dbData == null? null: dbData.toLocalDate();
    }

}
