package com.isc.stuinfo.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.isc.stuinfo.domain.jackson.CourseJsonSerialize;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:33:26 PM
 */
@Entity
@Table(name = "course", uniqueConstraints = @UniqueConstraint(columnNames = { "codeNumber", "codeName" }))
@JsonSerialize(using = CourseJsonSerialize.class)
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "codeName")
	@NotNull
	@NotEmpty
	@Pattern(regexp = "[a-zA-Z]+")
	private String codeName;

	@Column(name = "codeNumber")
	@NotNull
	@NotEmpty
	@Pattern(regexp = "[0-9]+")
	private String codeNumber;

	@Column(name = "name")
	@NotNull
	@NotEmpty
	private String name;

	@Column(name = "credits")
	@Min(value = 1)
	private int credits;

	@Column(name = "cost")
	@Min(value = 1)
	private double cost;

	@ManyToOne
	@JoinColumn(name = "courseTypeId")
	@NotNull
	private CourseType courseType;
	
	@ManyToMany
	@JoinTable(name = "prerequisite", joinColumns = @JoinColumn(name = "courseId"), inverseJoinColumns = @JoinColumn(name = "prerequisiteId"))
	private Set<Course> prerequitsite = new HashSet<>();

	public Course() {

	}

	@SuppressWarnings("unchecked")
	@JsonCreator // Parsing json to instance
	public Course(Map<String, Object> props) {
		this.codeName = (String) props.get("codeName");
		this.codeNumber = (String) props.get("codeNumber");
		this.name = (String) props.get("name");
		this.credits = (int) props.get("credits");
		this.cost = (double) props.get("cost");

		CourseType courseType = new CourseType();
		courseType.setId((int) props.get("courseType"));
		this.courseType = courseType;

		List<Integer> prerequisiteIds = (List<Integer>) props.get("prerequisites");
		prerequisiteIds.forEach((id) -> {
			Course course = new Course();
			course.setId(id);
			this.prerequitsite.add(course);
		});

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeNumber() {
		return codeNumber;
	}

	public void setCodeNumber(String codeNumber) {
		this.codeNumber = codeNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public CourseType getCourseType() {
		return courseType;
	}

	public void setCourseType(CourseType courseType) {
		this.courseType = courseType;
	}

	public Set<Course> getPrerequitsite() {
		return prerequitsite;
	}

	public void setPrerequitsite(Set<Course> prerequitsite) {
		this.prerequitsite = prerequitsite;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (id != other.id)
			return false;
		return true;
	}

}