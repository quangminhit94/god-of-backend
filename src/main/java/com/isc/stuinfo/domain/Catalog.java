package com.isc.stuinfo.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:33:26 PM
 */
@Entity
@Table(name = "catalog")
public class Catalog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "name")
	@NotNull
	@NotEmpty
	private String name;
	
	@Column(name = "note")
	@NotNull
	@NotEmpty
	private String note;
	
	@ManyToMany
	@JoinTable(
		name = "catalogCourse",
		joinColumns = @JoinColumn(name = "catalogId"),
		inverseJoinColumns = @JoinColumn(name = "courseId")
	)
	private Set<Course> courses = new HashSet<>();
	
	public Catalog() {

	}
	
	@SuppressWarnings("unchecked")
	@JsonCreator
	public Catalog(Map<String, Object> props) {
		this.name = (String)props.get("name");
		this.note = (String)props.get("note");
		
		List<Integer> ids = (List<Integer>)props.get("courses");
		ids.forEach((id) -> {
			Course course = new Course();
			course.setId(id);
			courses.add(course);
		});
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	
	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Catalog other = (Catalog) obj;
		if (id != other.id)
			return false;
		return true;
	}

}