package com.isc.stuinfo.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:33:38 PM
 */
@Entity
@Table(name = "person")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person implements HasImage {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected long id;
	
	@Column(name = "name")
	@NotNull
	@NotEmpty
	protected String name;
	
	@Column(name = "image")
	protected String image;
	
	@Column(name = "gender")
	protected boolean gender;
	
	@Column(name = "birthday")
	@NotNull
	protected Date birthday;
	
	@Column(name = "phone")
	@NotNull
	@NotEmpty
	protected String phone;
	
	@Column(name = "email")
	@NotNull
	@NotEmpty
	protected String email;
	
	@Embedded
	@NotNull
	protected Address address;
	
	@Embedded
	@AttributeOverrides(value={
		@AttributeOverride(name = "number",column=@Column(name="pAddressNumber")),
		@AttributeOverride(name = "ward",column=@Column(name = "pWard")),
		@AttributeOverride(name = "district",column=@Column(name = "pDistrict")),
		@AttributeOverride(name = "city",column=@Column(name = "pCity")),
		@AttributeOverride(name = "nationality",column=@Column(name = "pNationality"))
	})
	@NotNull
	protected Address postalAddress;
	
	@SuppressWarnings("unchecked")
	public Person(Map<String, Object> props) throws ParseException {
		this.name = (String)props.get("name");
		this.gender = (int)props.get("gender") == 1? false: true;
		this.phone = (String)props.get("phone");
		this.email = (String)props.get("email");
		
		this.address = parseAddress((Map<String, Object>)props.get("address"));
		this.postalAddress = parseAddress((Map<String, Object>)props.get("pAddress"));
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		this.birthday = simpleDateFormat.parse((String)props.get("birthday"));
	}
	
	// Parse json address to address object
	public Address parseAddress(Map<String, Object> props) {
		Address tempAddress = new Address();
		tempAddress.setCity((String) props.get("city"));
		tempAddress.setDistrict((String) props.get("district"));
		tempAddress.setNationality((String) props.get("nationality"));
		tempAddress.setNumber((String) props.get("number"));
		tempAddress.setWard((String) props.get("ward"));
		return tempAddress;
	}

	public Person() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getImage() {
		return image;
	}

	@Override
	public void setImage(String image) {
		this.image = image;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(Address postalAddress) {
		this.postalAddress = postalAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}