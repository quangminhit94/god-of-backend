package com.isc.stuinfo.domain;

public interface HasImage {
	public String getImage();
	public void setImage(String image);

}
