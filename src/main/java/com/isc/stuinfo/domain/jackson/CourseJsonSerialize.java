package com.isc.stuinfo.domain.jackson;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.isc.stuinfo.domain.Course;

public class CourseJsonSerialize extends StdSerializer<Course> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2085862955506291243L;

	protected CourseJsonSerialize() {
		super(Course.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void serialize(Course value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		// TODO Auto-generated method stub
		gen.writeStartObject();
			this.writeBasicInfo(value, gen);
			gen.writeFieldName("prerequisites");
			gen.writeStartArray();
				if(value.getPrerequitsite() != null) {
					value.getPrerequitsite().forEach((inst) -> {
						try {
							gen.writeStartObject();
							writeBasicInfo(inst, gen);
							gen.writeEndObject();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				}
			gen.writeEndArray();
		gen.writeEndObject();
	}
	
	// Write common info
	private void writeBasicInfo(Course value, JsonGenerator gen) throws IOException {
		gen.writeNumberField("id", value.getId());
		gen.writeStringField("codeName", value.getCodeName());
		gen.writeStringField("codeNumber", value.getCodeNumber());
		gen.writeStringField("name", value.getName());
		gen.writeNumberField("credits", value.getCredits());
		gen.writeNumberField("cost", value.getCost());
		gen.writeObjectField("courseType", value.getCourseType());
	}
}
