package com.isc.stuinfo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:33:26 PM
 */
@Entity
@Table(name = "programe")
public class Programe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "specCode")
	private String specCode;
	
	@Column(name = "specName")
	private String specName;
	
	@Column(name = "rCredit")
	private int rCredit;
	
	@Column(name = "eCredit")
	private int eCredit;

	public Programe() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getSpecName() {
		return specName;
	}

	public void setSpecName(String specName) {
		this.specName = specName;
	}

	public int getrCredit() {
		return rCredit;
	}

	public void setrCredit(int rCredit) {
		this.rCredit = rCredit;
	}

	public int geteCredit() {
		return eCredit;
	}

	public void seteCredit(int eCredit) {
		this.eCredit = eCredit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Programe other = (Programe) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}