package com.isc.stuinfo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author QuangHuy TienPhat VietCa
 * @version 1.0
 * @created 13-Apr-2017 3:33:36 PM
 */
@Entity
@Table(name = "class")
public class Class {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "semesterId")
	private Semester semester;
	
	@ManyToOne
	@JoinColumn(name = "academicYearId")
	private AcademicYear academicYear;
	
	@ManyToOne
	@JoinColumn(name = "courseId")
	private Course course;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "crn")
	private String crn;
	
	@Column(name = "status")
	private boolean status;
	
	@Column(name = "totalWeeks")
	private int totalWeeks;
	
	@Column(name = "totalHours")
	private int totalHours;
	
	@Column(name = "capacity")
	private int capacity;
	
	@Column(name = "maxAbsent")
	private double maxAbsent;
	
	@Column(name = "midPercent")
	private double midPercent;
	
	@Column(name = "finalPercent")
	private double finalPercent;

	public Class() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getTotalWeeks() {
		return totalWeeks;
	}

	public void setTotalWeeks(int totalWeeks) {
		this.totalWeeks = totalWeeks;
	}

	public int getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(int totalHours) {
		this.totalHours = totalHours;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public double getMaxAbsent() {
		return maxAbsent;
	}

	public void setMaxAbsent(double maxAbsent) {
		this.maxAbsent = maxAbsent;
	}

	public double getMidPercent() {
		return midPercent;
	}

	public void setMidPercent(double midPercent) {
		this.midPercent = midPercent;
	}

	public double getFinalPercent() {
		return finalPercent;
	}

	public void setFinalPercent(double finalPercent) {
		this.finalPercent = finalPercent;
	}

	public Class createLike() {
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Class other = (Class) obj;
		if (id != other.id)
			return false;
		return true;
	}

}