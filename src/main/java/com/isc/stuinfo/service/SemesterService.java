package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Semester;

public interface SemesterService extends CrudService<Semester, Long>{

}
