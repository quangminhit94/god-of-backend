package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.RoomType;

public interface RoomTypeService extends CrudService<RoomType, Long>{

}
