package com.isc.stuinfo.service;

import org.springframework.web.multipart.MultipartFile;

import com.isc.stuinfo.domain.HasImage;

public interface ImageService {
	public void save(HasImage emtity, MultipartFile file);
}
