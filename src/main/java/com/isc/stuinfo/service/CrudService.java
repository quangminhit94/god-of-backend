package com.isc.stuinfo.service;

import java.io.Serializable;

import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;

/**
 * 
 * @author tienp
 *
 * @param <T>
 * @param <I>
 */
public interface CrudService<T, I extends Serializable> {
	
	/**
	 * 
	 * @return
	 */
	public Iterable<T> findAll();
	
	/**
	 * 
	 * @param sortRequest
	 * @return
	 */
	public Iterable<T> findAll(SortRequest sortRequest);
	
	/**
	 * 
	 * @param pageRequest
	 * @return
	 */
	public Page<T> findAll(PageRequest pageRequest);
	
	/**
	 * 
	 * @param pageRequest
	 * @param sortRequest
	 * @return
	 */
	public Page<T> findAll(PageRequest pageRequest, SortRequest sortRequest);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public T findOne(I id);
	
	/**
	 * 
	 * @param entity
	 */
	public void save(T entity);
	
	/**
	 * 
	 * @param entity
	 */
	public void update(T entity);
	
	/**
	 * 
	 * @param id
	 */
	public void remove(I id);
	
	/**
	 * 
	 * @param entity
	 */
	public void remove(T entity);
}
