package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Instructor;

public interface InstructorService extends CrudService<Instructor, Long> {

}
