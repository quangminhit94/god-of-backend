package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Intake;

public interface IntakeService extends CrudService<Intake, Long> {

}
