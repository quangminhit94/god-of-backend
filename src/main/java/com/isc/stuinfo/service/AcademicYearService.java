package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.AcademicYear;

public interface AcademicYearService extends CrudService<AcademicYear, Long> {

}
