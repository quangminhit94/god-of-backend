package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Major;

public interface MajorService extends CrudService<Major, Long> {

}
