package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.OpenCourse;

public interface OpenCourseService extends CrudService<OpenCourse, Long> {

}
