package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.CourseType;

public interface CourseTypeService extends CrudService<CourseType, Long>{

}
