package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;
import com.isc.stuinfo.domain.Instructor;
import com.isc.stuinfo.service.InstructorService;

@Service
public class InstructorServiceImpl extends AbstractCrudService<Instructor, Long> implements InstructorService {

}
