package com.isc.stuinfo.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.isc.stuinfo.domain.HasImage;
import com.isc.stuinfo.service.ImageService;

@Service
public class ImageServiceImpl implements ImageService {
	
	@Value("${file.saveLocation}")
	private String saveLocation;
	
	@Override
	public void save(HasImage entity, MultipartFile file) {
		// TODO Auto-generated method stub
		try {
			// Set new unique file name
			String fileName = UUID.randomUUID() + "." +
					FilenameUtils.getExtension(file.getOriginalFilename());
			
			// Save image into filesystem
			File data = new File(this.saveLocation + "/" + fileName);
			file.transferTo(data);
			
			// Set image name into instance
			entity.setImage(fileName);
		} catch (IllegalStateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
