package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.OpenCourse;
import com.isc.stuinfo.service.OpenCourseService;

@Service
public class OpenCourseServiceImpl extends AbstractCrudService<OpenCourse, Long> implements OpenCourseService {

}
