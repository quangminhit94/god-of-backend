package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;
import com.isc.stuinfo.domain.Course;
import com.isc.stuinfo.service.CourseService;

@Service
public class CourseServiceImpl extends AbstractCrudService<Course, Long>
		implements CourseService {

}
