package com.isc.stuinfo.service.impl;

import com.isc.stuinfo.domain.Programe;
import com.isc.stuinfo.service.ProgrameService;

import org.springframework.stereotype.Service;

@Service
public class ProgrameServiceImpl extends AbstractCrudService<Programe, Long> implements ProgrameService{

}
