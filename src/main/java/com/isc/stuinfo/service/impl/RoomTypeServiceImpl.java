package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.RoomType;
import com.isc.stuinfo.service.RoomTypeService;

@Service
public class RoomTypeServiceImpl 
		extends AbstractCrudService<RoomType, Long> 
		implements RoomTypeService{

}
