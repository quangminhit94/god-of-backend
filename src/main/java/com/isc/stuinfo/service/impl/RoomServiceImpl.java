package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.Room;
import com.isc.stuinfo.service.RoomService;

@Service
public class RoomServiceImpl extends AbstractCrudService<Room, Long> implements RoomService {

}
