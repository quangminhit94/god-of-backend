package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;
import com.isc.stuinfo.domain.Class;
import com.isc.stuinfo.service.ClassService;

@Service
public class ClassServiceImpl extends AbstractCrudService<Class, Long>
		implements ClassService {

}
