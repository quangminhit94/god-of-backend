package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.AcademicYear;
import com.isc.stuinfo.service.AcademicYearService;

@Service
public class AcademicYearServiceImpl extends
		AbstractCrudService<AcademicYear, Long> implements AcademicYearService {

}
