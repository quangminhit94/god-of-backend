package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;
import com.isc.stuinfo.domain.CourseType;
import com.isc.stuinfo.service.CourseTypeService;

@Service
public class CourseTypeServiceImpl extends
		AbstractCrudService<CourseType, Long> implements CourseTypeService {

}
