package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.Student;
import com.isc.stuinfo.service.StudentService;

@Service
public class StudentServiceImpl 
		extends AbstractCrudService<Student, Long> 
		implements StudentService {

}
