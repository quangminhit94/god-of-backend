package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.Major;
import com.isc.stuinfo.service.MajorService;

@Service
public class MajorServiceImpl extends AbstractCrudService<Major, Long> implements MajorService {

}
