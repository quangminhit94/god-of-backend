package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.Semester;
import com.isc.stuinfo.service.SemesterService;

@Service
public class SemesterServiceImpl extends AbstractCrudService<Semester, Long> implements SemesterService {

}
