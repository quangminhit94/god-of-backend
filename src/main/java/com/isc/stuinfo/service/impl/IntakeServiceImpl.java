package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.Intake;
import com.isc.stuinfo.service.IntakeService;

@Service
public class IntakeServiceImpl extends AbstractCrudService<Intake, Long> implements IntakeService {

}
