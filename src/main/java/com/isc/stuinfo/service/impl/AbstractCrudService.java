package com.isc.stuinfo.service.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import com.isc.stuinfo.repository.CrudRepository;
import com.isc.stuinfo.repository.domain.Page;
import com.isc.stuinfo.repository.domain.PageRequest;
import com.isc.stuinfo.repository.domain.SortRequest;
import com.isc.stuinfo.service.CrudService;

public abstract class AbstractCrudService<T, I extends Serializable> implements CrudService<T, I> {

	@Autowired
	private CrudRepository<T, I> crudRepository;

	@Override
	public Iterable<T> findAll() {
		// TODO Auto-generated method stub
		return crudRepository.findAll();
	}

	@Override
	public Iterable<T> findAll(SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return crudRepository.findAll(sortRequest);
	}

	@Override
	public Page<T> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return crudRepository.findAll(pageRequest);
	}

	@Override
	public Page<T> findAll(PageRequest pageRequest, SortRequest sortRequest) {
		// TODO Auto-generated method stub
		return crudRepository.findAll(pageRequest, sortRequest);
	}

	@Override
	public T findOne(I id) {
		// TODO Auto-generated method stub
		return crudRepository.findOne(id);
	}

	@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
		crudRepository.save(entity);
	}

	@Override
	public void update(T entity) {
		// TODO Auto-generated method stub
		crudRepository.update(entity);
	}

	@Override
	public void remove(I id) {
		// TODO Auto-generated method stub
		crudRepository.remove(id);
	}

	@Override
	public void remove(T entity) {
		// TODO Auto-generated method stub
		crudRepository.remove(entity);
	}

}
