package com.isc.stuinfo.service.impl;

import org.springframework.stereotype.Service;

import com.isc.stuinfo.domain.Catalog;
import com.isc.stuinfo.service.CatalogService;

@Service
public class CatalogServiceImpl extends AbstractCrudService<Catalog, Long> implements CatalogService {

}
