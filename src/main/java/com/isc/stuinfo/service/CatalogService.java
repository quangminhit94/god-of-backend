package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Catalog;

public interface CatalogService extends CrudService<Catalog, Long> {

}
