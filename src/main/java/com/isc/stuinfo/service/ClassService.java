package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Class;

public interface ClassService extends CrudService<Class, Long> {

}
