package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Course;

public interface CourseService extends CrudService<Course, Long>{

}
