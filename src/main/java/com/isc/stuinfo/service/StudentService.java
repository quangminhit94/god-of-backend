package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Student;

public interface StudentService extends CrudService<Student, Long>{

}
