package com.isc.stuinfo.service;

import com.isc.stuinfo.domain.Room;

public interface RoomService extends CrudService<Room, Long> {

}
